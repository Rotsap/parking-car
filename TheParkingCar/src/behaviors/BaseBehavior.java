package behaviors;

import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.subsumption.Behavior;

public class BaseBehavior implements Behavior{
	private boolean _suppressed = false;
	private boolean inControl = true;
	NXTRegulatedMotor motorC;
	NXTRegulatedMotor motorB;
    private UltrasonicSensor us;
    private double axLength, taco;
	
	public BaseBehavior(double axelLength, double tacoToMm)
    {
		axLength = axelLength;
		taco = tacoToMm;
    	us = new UltrasonicSensor(SensorPort.S2);
		motorC = new NXTRegulatedMotor(MotorPort.C);
		motorB = new NXTRegulatedMotor(MotorPort.B);
		motorC.setSpeed(80);
		motorB.setSpeed(80);
    } 
	
	@Override
	public boolean takeControl() {
		return inControl;
	}

	@Override
	public void action() {
		_suppressed = false;
		while (! _suppressed){
			//look for a parking space
			while(us.getDistance() < 20){
				driveForward();
			}
			stop();
			motorB.resetTachoCount();
			
			//when found a space register the length of space
			while(us.getDistance() > 20){
				
				driveForward();
			}
			
			stop();
			isRoom(motorB.getTachoCount());
			
			
			 
		}
	}
	public void isRoom(double tacoCount){
		if(-(tacoCount) * taco > (axLength*2.5) ){
			_suppressed = true;
			inControl = false;
		}
	}

	@Override
	public void suppress() {
		_suppressed = true;
		
	}
	private void stop(){
		motorC.stop();
		motorB.stop();
	}
	private void driveForward(){
		motorC.backward();
		motorB.backward();
	}
	
	

}
