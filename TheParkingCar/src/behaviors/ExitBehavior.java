package behaviors;

import lejos.nxt.Button;
import lejos.robotics.subsumption.Behavior;

public class ExitBehavior implements Behavior{

	private boolean suppressed = false;
	
	@Override
	public boolean takeControl() {
				
		return (Button.ESCAPE.isDown());
	}

	@Override
	public void action() {
		System.exit(0);
	}

	@Override
	public void suppress() {
		setSuppressed(true);
	}

	public boolean isSuppressed() {
		return suppressed;
	}

	public void setSuppressed(boolean suppressed) {
		this.suppressed = suppressed;
	}

}