package behaviors;

import lejos.nxt.Motor;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.Sound;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Behavior;

public class ParkingBehavior  implements Behavior {
	private boolean _suppressed = false;
    private double axLength, taco;
    private double diameter = 91.00;
	public int maxTurn = 80; // magisk tal fundet ved test - positiv drejer til h�jre og negativ drejer til venstre
	DifferentialPilot pilot;
	
	public ParkingBehavior(double axelLength, double tacoToMm)
    {
		axLength = axelLength;
		taco = tacoToMm;
		
		pilot = new DifferentialPilot(DifferentialPilot.WHEEL_SIZE_NXT2, 6.02362205f, Motor.B, Motor.C, true);
		
		pilot.setTravelSpeed(2.5);
	}
	
	@Override
	public void action() {
		_suppressed = false;
		if (! _suppressed){
			getInPosition();
			reversInToSpace();
			
		}
		System.exit(0);
	}

	private void getInPosition(){
		stop();
		sleep(1000);
		
		pilot.travel(cmToInch(axLength));
			Sound.beepSequenceUp(); 
	}
	
	public void reversInToSpace(){
		turn(maxTurn);
		sleep(1000);
		double degree = calc45degrees(diameter);
		parkingBackward(cmToInch(degree));
		turn(-maxTurn-10);//drejer hjulene tilbaget til start position(ish) de ekstra 10 er ca det sl�r der er i Legoet
		sleep(1500);
		parkingBackward(cmToInch(axLength));//dette er vurderet til at passe s�dan da eller n�sten der omkring, m�ske
		turn(-maxTurn);
		sleep(1500);
		parkingBackward(cmToInch(14));
		turn(maxTurn+10);
		sleep(1500);
		parkingForward(cmToInch(10));
		Sound.beepSequenceUp(); 
	}
	
	public double cmToInch(double cm){
		
		double result = cm * 0.3937;//omregner til inches 
		return result;
	}

	public double calc45degrees(double dia){
		double omkres = dia * Math.PI;
		double result = (omkres/360)*22.5;
		return result;
	}
	
	public void sleep(int msec) {
		try {
			Thread.sleep(msec);
		}
		catch(Exception E){
		
		}
	}
	
	
	public void stop(){
		pilot.stop();
	}
		
	public void parkingForward(double dist){
		pilot.travel(dist);
	}
	
	public void parkingBackward(double dist){
		pilot.travel(-dist);
	}
	
	
	public float travledDist(){
		return pilot.getMovementIncrement();
	}
		
	public void turn(int angle){
		Motor.A.setAcceleration(60);
		Motor.A.setSpeed(100);
		Motor.A.rotate(angle, true);
	}
	
	
	
	@Override
	public boolean takeControl() {
		return true;
	}

	

	@Override
	public void suppress() {
		_suppressed = true;
		
	}

}
