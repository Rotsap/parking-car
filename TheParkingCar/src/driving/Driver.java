package driving;

import lejos.nxt.Motor; 
import lejos.robotics.navigation.DifferentialPilot;

public class Driver {
	DifferentialPilot pilot;
	public Driver(){
		pilot = new DifferentialPilot(DifferentialPilot.WHEEL_SIZE_NXT2, 6.02362205f, Motor.B, Motor.C, true);
		Motor.C.setAcceleration(20);
		Motor.B.setAcceleration(20);
	}
	
	public void stop(){
		pilot.stop();
	}
		
	public void parkingForward(double dist){
		pilot.travel(dist);
	}
	
	public void parkingBackward(double dist){
		pilot.travel(-dist);
	}
	
	public void driveForward(){
		pilot.forward();
	}
	
	public void driveBackward(){
		pilot.backward();
	}
	
	public float travledDist(){
		return pilot.getMovementIncrement();
	}
		
	public void turn(int angle){
		Motor.A.setAcceleration(60);
		Motor.A.setSpeed(100);
		Motor.A.rotate(angle, true);
	}
	
	
}
