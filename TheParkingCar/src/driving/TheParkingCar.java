package driving;


import behaviors.BaseBehavior;
import behaviors.ExitBehavior;
import behaviors.ParkingBehavior;
import lejos.nxt.*;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class TheParkingCar{
	public static double AXELDISTANCE = 17.6;
	public static double TACOCOUNTTOMM = 0.12;
	public static Navigator navi = new Navigator();
	public static void main(String [] args){
		/*TheParkingCar listener = new TheParkingCar();
		Button.ESCAPE.addButtonListener(listener);
		
			navi.runTime();*/
		Behavior b2 = new BaseBehavior(AXELDISTANCE, TACOCOUNTTOMM);
		Behavior b1 = new ParkingBehavior(AXELDISTANCE, TACOCOUNTTOMM);
		Behavior b3 = new ExitBehavior();
		
		Behavior[] behaviorList = { b1, b2, b3 };
		Arbitrator arbitrator = new Arbitrator(behaviorList);
		
		Button.waitForAnyPress();
		
		arbitrator.start();
	}
	

}