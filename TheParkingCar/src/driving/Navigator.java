package driving;

import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;


public class Navigator {
	private double diameter = 91.00;
	public int maxTurn = 80; // magisk tal fundet ved test - positiv drejer til h�jre og negativ drejer til venstre
	private UltrasonicSensor us;
	public Driver driver;
	
	public Navigator(){
		
		us = new UltrasonicSensor(SensorPort.S1);
		
		driver = new Driver();
		
	}
	
	public void runTime(){
		
		reversInToSpace();
		while(true){
			
			System.out.println(us.getDistance());
		}
	}
	
	
	
	public void reversInToSpace(){
		driver.turn(maxTurn);
		sleep(1000);
		double degree = calc45degrees(diameter);
		driver.parkingBackward(cmToInch(degree));
		driver.turn(-maxTurn-10);//drejer hjulene tilbaget til start position(ish) de ekstra 10 er ca det sl�r der er i Legoet
		sleep(1000);
		driver.parkingBackward(cmToInch(15));//magisk tal, for at teste, skal beregnes 
		driver.turn(-maxTurn);
		sleep(1000);
		driver.parkingBackward(cmToInch(15));
		driver.turn(maxTurn+10);
		sleep(1000);
		driver.parkingForward(cmToInch(15));
		Sound.beepSequenceUp(); 
	}
	
	public double cmToInch(double cm){
		
		double result = cm * 0.3937;//omregner til inches 
		return result;
	}

	public double calc45degrees(double dia){
		double omkres = dia * Math.PI;
		double result = (omkres/360)*22.5;
		return result;
	}
	public void sleep(int msec) {
		try {
			Thread.sleep(msec);
		}
		catch(Exception E){
		
		}
	}
}

